<?php
namespace App\Tests\Services;

use App\Entity\User;
use App\Services\UserService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserServiceTest extends TestCase
{
    private $userService;
    private $passwordHasher;
    private $validator;
    private $serializer;

    protected function setUp(): void
    {
        $this->passwordHasher = $this->createMock(UserPasswordHasherInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);

        $this->userService = new UserService(
            $this->passwordHasher,
            $this->validator,
            $this->serializer
        );
    }

    public function testCreateUser(): void
    {
        $requestData = '{"username": "test_user", "email": "test@example.com"}';
        $user = new User();
        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($requestData, User::class, 'json')
            ->willReturn($user);

        $result = $this->userService->createUser($requestData);

        $this->assertSame($user, $result);
    }

    public function testUpdateUser(): void
    {
        $requestData = '{"username": "updated_user"}';
        $user = new User();
        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($requestData, User::class, 'json', ['object_to_populate' => $user, 'ignored_attributes' => ['password']])
            ->willReturn($user);

        $result = $this->userService->updateUser($user, $requestData);

        $this->assertSame($user, $result);
    }
    
     protected function tearDown(): void
     {
         $this->userService = null;
         $this->passwordHasher = null;
         $this->validator = null;
         $this->serializer = null;
     }
}