<?php
namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof NotFoundHttpException) {
            $response = new JsonResponse(['error' => 'Invalid endpoint'], 404);
        } else {
            $response = new JsonResponse(
                ['error' => $exception->getMessage()],
                $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : 500
            );
        }

        $event->setResponse($response);
    }
}