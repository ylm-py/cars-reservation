<?php

namespace App\Controller;

use App\Services\CarService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\ExceptionHandler;



class CarController extends AbstractController
{
    private CarService $carService;
    private ExceptionHandler $exceptionHandler;

    

    public function __construct(
        CarService $carService,
        ExceptionHandler $exceptionHandler
    ) {
        $this->carService = $carService;
        $this->exceptionHandler = $exceptionHandler;
    }

    #[Route('/cars', name: 'cars_get_all', methods: ['GET'])]
    public function list(EntityManagerInterface $entityManager, Request $request, PaginatorInterface $paginator): JsonResponse
    {
        try{
            $data = $this->carService->getPaginatedCars($paginator, $request, $entityManager);
            return new JsonResponse($data, Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_retreive');
        }
    }

    #[Route('/cars', name: 'cars_create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $car = $this->carService->createCar($request->getContent());

            $errors = $this->carService->validateCar($car);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager->persist($car);
            $entityManager->flush();

            return new JsonResponse($car->jsonSerialize(), Response::HTTP_CREATED);
        }catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_data_invalid');
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_create');
        }
    }

    #[Route('/cars/{id}', name: 'cars_get', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getOne(int $id): JsonResponse
    {
        try{
            $car = $this->carService->checkCarExists($id);
            if (!$car) {
                return new JsonResponse(['error' => 'Car not found'], Response::HTTP_NOT_FOUND);
            }

            return new JsonResponse($car->jsonSerialize(), Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.car_retreive');
        }
    }

    #[Route('/cars/{id}', name: 'cars_update', methods: ['PUT'], requirements: ['id' => '\d+'])]
    public function update(int $id, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $car = $this->carService->checkCarExists($id);
            if (!$car) {
                return new JsonResponse(['error' => 'Car not found'], Response::HTTP_NOT_FOUND);
            }
            $car = $this->carService->updateCar($car, $request->getContent());

            $errors = $this->carService->validateCar($car);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager->persist($car);
            $entityManager->flush();

            return new JsonResponse($car->jsonSerialize(), Response::HTTP_OK);
    
        }catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_data_invalid');
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_update');
        }
    }

    #[Route('/cars/{id}', name: 'cars_delete', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function delete(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $car = $this->carService->checkCarExists($id);
            if (!$car) {
                return new JsonResponse(['error' => 'Car not found'], Response::HTTP_NOT_FOUND);
            }

            $entityManager->remove($car);
            $entityManager->flush();

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.cars_delete');
        }
    }
    
}
