<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use App\Services\UserService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\ExceptionHandler;



class UserController extends AbstractController
{

    private UserService $userService;
    private LoggerInterface $logger;

    private TranslatorInterface $translator;

    private ExceptionHandler $exceptionHandler;

    public function __construct(
        UserService $userService, 
        LoggerInterface $logger,
        TranslatorInterface $translator,
        ExceptionHandler $exceptionHandler
    ) {
        $this->userService = $userService;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->exceptionHandler = $exceptionHandler;
    }

    #[Route('/users', name: 'users_get_all', methods: ['GET'])]
    public function getAll(EntityManagerInterface $entityManager, Request $request, PaginatorInterface $paginator): JsonResponse
    {
        try{
            $data = $this->userService->getPaginatedUsers($paginator, $request, $entityManager);
            return new JsonResponse($data, Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.users_retreive');
        }
    }
    #[Route('/users', name: 'users_create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->createUser($request->getContent());

            $errors = $this->userService->validateUser($user);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }

            $this->userService->hashUserPassword($user, $user->getPassword());
            
            $entityManager->persist($user);
            $entityManager->flush();
            return new JsonResponse($user->jsonSerialize(), Response::HTTP_CREATED);

    
        }catch(UniqueConstraintViolationException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_unique_constraint');
        }catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_data_invalid');
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_create');
        }
    }

    #[Route('/users/{id}', name: 'users_get', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getOne(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->checkUserExists($id, $entityManager);
            if (!$user) {
                return new JsonResponse(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
            return new JsonResponse($user->jsonSerialize(), Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_retreive');
        }
    }


    #[Route('/users/{id}', name: 'users_update', methods: ['PUT'], requirements: ['id' => '\d+'])]
    public function update(int $id, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->checkUserExists($id, $entityManager);
            if (!$user) {
                return new JsonResponse(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
            $updatedUser = $this->userService->updateUser($user, $request->getContent());
            $errors = $this->userService->validateUser($updatedUser);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }
            $entityManager->persist($updatedUser);
            $entityManager->flush();
            return new JsonResponse($user->jsonSerialize(), Response::HTTP_OK);
        }catch(UniqueConstraintViolationException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_unique_constraint');
        }catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_data_invalid');
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_update');
        }
    }

    #[Route('/users/{id}', name: 'users_delete', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function delete(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->checkUserExists($id, $entityManager);
            if (!$user) {
                return new JsonResponse(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
            $entityManager->remove($user);
            $entityManager->flush();
            return new JsonResponse(['message' => 'User deleted'], Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_delete');
        }
    }

    #[Route('/users/{id}/change-password', name: 'users_change_password', methods: ['PUT'], requirements: ['id' => '\d+'])]
    public function changePassword(int $id, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->checkUserExists($id, $entityManager);
            if (!$user) {
                return new JsonResponse(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
            $updatedUser = $this->userService->changePassword($user, $request->getContent());
            $errors = $this->userService->validateUser($updatedUser);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }
            $entityManager->persist($user);
            $entityManager->flush();
            return new JsonResponse(['message' => 'Password changed'], Response::HTTP_OK);
        }catch(\InvalidArgumentException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_password_change_data_invalid');
        }catch(NotEncodableValueException $e){

            return $this->exceptionHandler->handleException($e, 'errors.user_password_change_data_invalid');
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_password_change');
        }
    }

    #[Route('/users/{id}/reservations', name: 'users_get_reservations', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getReservations(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $user = $this->userService->checkUserExists($id, $entityManager);
            if (!$user) {
                return new JsonResponse(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
            $this->denyAccessUnlessGranted('USER_RESERVATION_VIEW', $user);
            $reservations = $user->getReservations();
            return new JsonResponse($reservations, Response::HTTP_OK);
        }catch(AccessDeniedException $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_reservations_retreive_forbidden');
        }
        catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.user_reservations_retreive');   
        }

    }

}