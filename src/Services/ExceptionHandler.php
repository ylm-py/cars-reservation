<?php
namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionHandler
{
    private $logger;
    private $translator;

    public function __construct(LoggerInterface $logger, TranslatorInterface $translator)
    {
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function handleException(\Throwable $exception, string $translateString): JsonResponse
    {
        $this->logger->error($exception->getMessage());

        $message = $this->getEnvironment() === 'dev' ? $exception->getMessage() : $this->translator->trans($translateString);

        return new JsonResponse(['error' => $message], $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : 500);
    }

    private function getEnvironment(): string
    {
        return $_SERVER['APP_ENV'] ?? 'prod';
    }
}