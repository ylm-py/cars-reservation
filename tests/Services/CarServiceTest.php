<?php

namespace App\Tests\Services;

use App\Entity\Car;
use App\Services\CarService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use DateTimeImmutable;

class CarServiceTest extends KernelTestCase
{
    private $validator;
    private $serializer;
    private $entityManager;
    private $carService;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);

        $this->carService = new CarService($this->validator, $this->serializer, $this->entityManager);
    }

    public function testCreateCar(): void
    {
        $jsonData = '{"brand":"dacia","model":"2029"}';

        $expectedCar = new Car();
        $expectedCar->setBrand('dacia');
        $expectedCar->setModel('2029');

        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($jsonData, Car::class, 'json')
            ->willReturn($expectedCar);

        $car = $this->carService->createCar($jsonData);

        $this->assertInstanceOf(Car::class, $car);
        $this->assertEquals('dacia', $car->getBrand());
        $this->assertEquals('2029', $car->getModel());
        $this->assertEquals('available', $car->getAvailability());
    }

    public function testUpdateCar(): void
    {
        $existingCar = new Car();
        $existingCar->setBrand('dacia');
        $existingCar->setModel('2029');
        $existingCar->setAvailability('available');

        $jsonData = '{"brand":"dacia","model":"2029","availability":"unavailable"}';

        $expectedCar = new Car();
        $expectedCar->setBrand('dacia');
        $expectedCar->setModel('2029');
        $expectedCar->setAvailability('unavailable');

        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($jsonData, Car::class, 'json', ['object_to_populate' => $existingCar])
            ->willReturn($expectedCar);

        $car = $this->carService->updateCar($existingCar, $jsonData);

        $this->assertInstanceOf(Car::class, $car);
        $this->assertEquals('dacia', $car->getBrand());
        $this->assertEquals('2029', $car->getModel());
        $this->assertEquals('unavailable', $car->getAvailability());
    }

    public function testValidateCarWithValidCar(): void
    {
        $car = (new Car())
            ->setBrand('Toyota')
            ->setModel('Camry')
            ->setAvailability('available');

        $this->validator->expects($this->once())
            ->method('validate')
            ->with($car)
            ->willReturn([]);

        $errorMessages = $this->carService->validateCar($car);

        $this->assertEmpty($errorMessages);
    }

    public function testIsCarAvailableWithNoReservations(): void
    {
        $car = new Car();
        $startDate = new DateTimeImmutable('2025-01-01');
        $endDate = new DateTimeImmutable('2025-01-05');

        $result = $this->carService->isCarAvailable($car, $startDate, $endDate, null);

        $this->assertTrue($result);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->validator = null;
        $this->serializer = null;
        $this->entityManager = null;
        $this->carService = null;
    }
}