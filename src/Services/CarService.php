<?php
namespace App\Services;

use App\Entity\Car;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use DateTimeImmutable;

class CarService 
{
    private $validator;
    private $serializer;
    private $entityManager;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    public function createCar($requestData): Car
    {
        $car = $this->serializer->deserialize($requestData, Car::class, 'json');
        return $car;
    }

    public function updateCar(Car $car, $requestData): Car
    {
        $car = $this->serializer->deserialize($requestData, Car::class, 'json', ['object_to_populate' => $car]);
        return $car;
    }

    public function checkCarExists(int $id): ?Car
    {
        $car = $this->entityManager->getRepository(Car::class)->find($id);
        if (!$car) {
            return null;
        }
        return $car;
    }

    public function validateCar(Car $car): array
    {
        try {
            $errors = $this->validator->validate($car);
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
            }
            return $errorMessages;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function isCarAvailable(Car $car, DateTimeImmutable $startDate, DateTimeImmutable $endDate, ?int $reservationId): bool
    {
        if (count($car->getReservations()) === 0) {
            return true;
        }
        foreach ($car->getReservations() as $reservation) {
            if ($reservation->getStatus() !== 'active' || $reservation->getId() === $reservationId) {
                continue;
            }
            $reservationStartDate = $reservation->getStartDate();
            $reservationEndDate = $reservation->getEndDate();

            if ($startDate >= $reservationStartDate && $startDate <= $reservationEndDate) {
                return false;
            }

            if ($endDate >= $reservationStartDate && $endDate <= $reservationEndDate) {
                return false;
            }

            if ($startDate <= $reservationStartDate && $endDate >= $reservationEndDate) {
                return false;
            }
        }

        return true;
    }

    public function getPaginatedCars($paginator, $request, $em): array
    {
        $query = $em->getRepository(Car::class)->createQueryBuilder('r')->getQuery();
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);
        $pagination = $paginator->paginate($query, $page, $limit);
        $itemsCount = $pagination->getTotalItemCount();

        $data = [];
        foreach ($pagination as $car) {
            $data[] = $car->jsonSerialize();
        }
        $data = [
            'items' => $data,
            'total' => $itemsCount,
        ];
        return $data;
        
    }

}
