<?php
namespace App\Services;

use App\Entity\Reservation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\CarService;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use App\Entity\Car;
use DateTimeImmutable;

class ReservationService
{
    private $validator;
    private $serializer;
    private $entityManager;

    private $carService;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        CarService $carService
    ) {
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->carService = $carService;
    }

    public function createReservation($requestData): Reservation
    {
        $context = ['carService' => $this->carService];
        $reservation = $this->serializer->deserialize($requestData, Reservation::class, 'json', $context);
        return $reservation;
    }

    public function updateReservation(Reservation $reservation, $requestData): Reservation
    {
        $context = ['carService' => $this->carService, 'object_to_populate' => $reservation];
        $reservation = $this->serializer->deserialize($requestData, Reservation::class, 'json', $context);
        return $reservation;
    }

    public function deleteReservation(Reservation $reservation): void
    {
        $this->entityManager->remove($reservation);
        $this->entityManager->flush();
    }

    public function validateReservation(Reservation $reservation): array
    {
        $errors = $this->validator->validate($reservation);
        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
        }
        return $errorMessages;
        
    }

    public function checkReservationExists(int $id): ?Reservation
    {
        $reservation = $this->entityManager->getRepository(Reservation::class)->find($id);
        if (!$reservation) {
            return null;
        }
        return $reservation;
    }

    public function validateCarAvailability(Reservation $reservation): void
    {
        $startDate = $reservation->getStartDate();
        $endDate = $reservation->getEndDate();
        $this->dateControl($startDate, $endDate);
        
        $car = $reservation->getCar();
        $isCarAvailable = $this->carService->isCarAvailable($car, $startDate, $endDate, $reservation->getId());
        if (!$isCarAvailable) {
            throw new \InvalidArgumentException('Car is not available for the selected dates.');
        }
    }

    public function dateControl(DateTimeImmutable $startDate, DateTimeImmutable $endDate): void
    {
        $currentDate = new \DateTime();
        if ($startDate > $endDate || $startDate < $currentDate || $endDate < $currentDate) {
            throw new \InvalidArgumentException('Invalid date range.');
        }
    }

    public function getPaginatedReservations($paginator, $request, $em): array
    {
        $query = $em->getRepository(Reservation::class)->createQueryBuilder('r')->getQuery();
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);
        
        $pagination = $paginator->paginate($query, $page, $limit);
        $itemCount = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($pagination as $reservation) {
            $data[] = $reservation->jsonSerialize();
        }
        $data = [
            'items' => $data,
            'total' => $itemCount,
        ];
        return $data;
    }

}