<?php
namespace App\Serializer;

use App\Entity\Reservation;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use DateTimeImmutable;

class ReservationNormalizer implements ContextAwareDenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = []): Reservation
    {
        

        if(isset($context['object_to_populate'])){
            $reservation = $context['object_to_populate'];
        }
        else{
            $reservation = new Reservation();
        }
        
        if (isset($data['start_date'])) {
            try {
                $startDate = DateTimeImmutable::createFromFormat('d/m/Y', $data['start_date']);
                if ($startDate === false) {
                    throw new \InvalidArgumentException('Invalid start_date format in reservation data.');
                }
                $reservation->setStartDate($startDate);
            } catch (\Exception $e) {
                throw new \InvalidArgumentException('Invalid start_date format in reservation data.');
            }
        }

        if (isset($data['end_date'])) {
            try {
                $endDate = DateTimeImmutable::createFromFormat('d/m/Y', $data['end_date']);
                if ($endDate === false) {
                    throw new \InvalidArgumentException('Invalid end_date format in reservation data.');
                }
                $reservation->setEndDate($endDate);
            } catch (\Exception $e) {
                throw new \InvalidArgumentException('Invalid end_date format in reservation data.');
            }
        }

        if (isset($data['car_id'])){
            $car = $context['carService']->checkCarExists($data['car_id']);
            if (!$car) {
                throw new \InvalidArgumentException('Car does not exist.');
            }
            $reservation->setCar($car);
        }
        return $reservation;
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return $type === Reservation::class;
    }
}