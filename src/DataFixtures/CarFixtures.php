<?php
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Car;

class CarFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 10; $i++) { 
            $car = new Car();
            $car->setBrand('Brand'.$i);
            $car->setModel('Model'.$i);
            $manager->persist($car);
        }
        $manager->flush();
    }
}