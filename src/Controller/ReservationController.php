<?php

namespace App\Controller;

use App\Services\ReservationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\ExceptionHandler;



class ReservationController extends AbstractController
{
    private ReservationService $reservationService;
    private ExceptionHandler $exceptionHandler;


    public function __construct(
        ReservationService $reservationService,
        ExceptionHandler $exceptionHandler
    ) {
        $this->reservationService = $reservationService;
        $this->exceptionHandler = $exceptionHandler;
    }
    #[Route('/reservations', name: 'reservations_get_all', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): JsonResponse
    {
        // $this->denyAccessUnlessGranted('ROLE_ADMIN'); //TODO : add role based access control for global reservations list
        try{
            $data = $this->reservationService->getPaginatedReservations($paginator, $request, $entityManager);
            return new JsonResponse($data, Response::HTTP_OK);
        }catch(\Exception $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservations_retreive');
        }
    }

    #[Route('/reservations', name: 'reservations_create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $reservation = $this->reservationService->createReservation($request->getContent());
            $errors = $this->reservationService->validateReservation($reservation);

            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }
            $this->reservationService->validateCarAvailability($reservation);
            $reservation->setUser($this->getUser());
            $entityManager->persist($reservation);
            $entityManager->flush();
            return new JsonResponse($reservation->jsonSerialize(), Response::HTTP_CREATED);
    
        }catch(\InvalidArgumentException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservations_data_invalid');
        }
        catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservations_create');
        }
    }

    #[Route('/reservations/{id}', name: 'reservations_get', methods: ['GET'], requirements: ['id' => '\d+'])]
    public function getOne(int $id): JsonResponse
    {
        try{
            $reservation = $this->reservationService->checkReservationExists($id);
            if (!$reservation) {
                return new JsonResponse(['error' => 'Reservation not found'], Response::HTTP_NOT_FOUND);
            }
            $this->denyAccessUnlessGranted('RESERVATION_VIEW', $reservation);
            return new JsonResponse($reservation->jsonSerialize(), Response::HTTP_OK);
        }catch(AccessDeniedException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservation_view_forbidden');
        }
    }

    #[Route('/reservations/{id}', name: 'reservations_update', methods: ['PUT'], requirements: ['id' => '\d+'])]
    public function update(int $id, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $reservation = $this->reservationService->checkReservationExists($id);
            if (!$reservation) {
                return new JsonResponse(['error' => 'Reservation not found'], Response::HTTP_NOT_FOUND);
            }
            $this->denyAccessUnlessGranted('RESERVATION_EDIT', $reservation);
            $reservation = $this->reservationService->updateReservation($reservation, $request->getContent());
            $errors = $this->reservationService->validateReservation($reservation);
            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }
            $this->reservationService->validateCarAvailability($reservation);
            $entityManager->persist($reservation);
            $entityManager->flush();
            return new JsonResponse($reservation->jsonSerialize(), Response::HTTP_OK);
    
        }catch(\InvalidArgumentException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservations_data_invalid');
        }catch(NotEncodableValueException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservation_update_forbidden');
        }catch(AccessDeniedException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservation_update_forbidden');
        }
    }

    #[Route('/reservations/{id}', name: 'reservations_delete', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    public function delete(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        try{
            $reservation = $this->reservationService->checkReservationExists($id);
            if (!$reservation) {
                return new JsonResponse(['error' => 'Reservation not found'], Response::HTTP_NOT_FOUND);
            }
            $this->denyAccessUnlessGranted('RESERVATION_CANCEL', $reservation);

            $reservation->cancel();
            $entityManager->persist($reservation);
            $entityManager->flush();
            return new JsonResponse(['message' => 'Reservation Canceled'], Response::HTTP_OK);
        }catch(AccessDeniedException $e){
            return $this->exceptionHandler->handleException($e, 'errors.reservation_cancel_forbidden');
        }
    }
}
