<?php
namespace App\Services;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class UserService
{
    private $passwordHasher;
    private $validator;
    private $serializer;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function createUser($requestData): User
    {
        $user = $this->serializer->deserialize($requestData, User::class, 'json');
        return $user;
    }

    public function updateUser(User $user, $requestData): User
    {
        $user = $this->serializer->deserialize($requestData, User::class, 'json', ['object_to_populate' => $user, 'ignored_attributes' => ['password']]);
        return $user;
    }

    public function changePassword(User $user, $requestData): User
    {
        $data= json_decode($requestData, true);
        $password = $data['password'];
        $passwordConfirm = $data['password_confirm'];
        if ($password !== $passwordConfirm) {
            throw new \InvalidArgumentException('Passwords do not match');
        }
        $this->hashUserPassword($user, $password);
        return $user;
    }

    public function validateUser(User $user): array
    {
        try{
            $errors = $this->validator->validate($user);
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
            }
            return $errorMessages;
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function hashUserPassword(User $user, string $password): void
    {
        $user->setPassword($this->passwordHasher->hashPassword($user, $password));
    }

    public function checkUserExists(int $id, EntityManagerInterface $entityManager): ?User
    {
        $user = $entityManager->getRepository(User::class)->find($id);
        if (!$user) {
            return null;
        }
        return $user;
    }

    public function getPaginatedUsers($paginator, $request, $em): array
    {
        $query = $em->getRepository(User::class)->createQueryBuilder('r')->getQuery();
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);
        $pagination = $paginator->paginate($query, $page, $limit);
        $itemsCount = $pagination->getTotalItemCount();
        
        $data = [];
        foreach ($pagination->getItems() as $item) {
            $data[] = $item->jsonSerialize();
        }
        $data = [
            'items' => $data,
            'totalItems' => $itemsCount,
        ];
        return $data;
    }
}