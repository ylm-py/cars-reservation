nelmio_api_doc:
  documentation:
    servers:
      - url: http://127.0.0.1:8000
        description: v1.0
    info:
      title: Car Reservation API
      description: This is a RESTful API for Car Reservation
      version: 1.0.0
    components:
      securitySchemes:
        Bearer:
          type: http
          scheme: bearer
          bearerFormat: JWT
    security:
      - Bearer: []
    # Define all paths below in separate files for better readability
    paths:
      /api/login_check:
        post:
          tags: [Authentication]
          summary: Login
          requestBody:
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    username:
                      type: string
                      example: user1
                    password:
                      type: string
                      example: "123456"
          responses:
            200:
              description: Login successful
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      token:
                        type: string
            401:
              description: Invalid credentials
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      message:
                        type: string
                      code:
                        type: integer
                      status:
                        type: integer
      /api/cars:
        get:
          tags: [Cars]
          summary: List Cars
          parameters:
            - name: page
              in: query
              required: false
              description: The page number
              schema:
                type: integer
                default: 1
            - name: limit
              in: query
              required: false
              description: The number of items per page
              schema:
                type: integer
                default: 10
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  example: |
                    {
                      "items": [
                        {
                          "id": 1,
                          "brand": "Toyota",
                          "model": "Camry"
                        },
                        {
                          "id": 2,
                          "brand": "Honda",
                          "model": "Civic"
                        }
                      ],
                      "total": 2
                    }
                    
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
        post:
          tags: [Cars]
          summary: Create Car
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    brand:
                      type: string
                      example: Toyota
                    model:
                      type: string
                      example: Camry
          responses:
            '201':
              description: Car created successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      brand:
                        type: string
                      model:
                        type: string
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '500':
              description: Server error
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
      /api/cars/{id}:
        get:
          tags: [Cars]
          summary: Get Car by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the car
              schema:
                type: integer
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  example: |
                    {
                      "id": 1,
                      "brand": "Toyota",
                      "model": "Camry"
                    }
            '404':
              description: Car not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Car not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
        put:
          tags: [Cars]
          summary: Update Car by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the car
              schema:
                type: integer
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    brand:
                      type: string
                      example: UpdatedBrand
                    model:
                      type: string
                      example: UpdatedModel
          responses:
            '200':
              description: Car updated successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      brand:
                        type: string
                      model:
                        type: string
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '404':
              description: Car not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Car not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
        delete:
          tags: [Cars]
          summary: Delete Car by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the car
              schema:
                type: integer
          responses:
            '204':
              description: Car deleted successfully
            '404':
              description: Car not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Car not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
      /api/users:
        get:
          tags: [Users]
          summary: List Users
          parameters:
            - name: page
              in: query
              required: false
              description: The page number
              schema:
                type: integer
                default: 1
            - name: limit
              in: query
              required: false
              description: The number of items per page
              schema:
                type: integer
                default: 10
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  example: |
                    {
                      "items": [
                        {
                          "id": 1,
                          "username": "user1",
                          "roles": [
                            "ROLE_USER"
                          ]
                        },
                        {
                          "id": 2,
                          "username": "user2",
                          "roles": [
                            "ROLE_USER"
                          ]
                        }
                      ],
                      "total": 2
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
        post:
          tags: [Users]
          summary: Create User
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    username:
                      type: string
                      example: user1
                    password:
                      type: string
                      example: "123456"
                    email:
                      type: string
                      example: "valid@email.com"
                    firstName:
                      type: string
                      example: Frank
                    lastName:
                      type: string
                      example: Castle
          responses:
            '201':
              description: User created successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      username:
                        type: string
                      roles:
                        type: array
                        items:
                          type: string
                      first_name:
                        type: string
                      last_name:
                        type: string
                      email:
                        type: string
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      first_name:
                        type: array
                        items:
                          type: string
                          example: "This value should not be blank."
                      last_name:
                        type: array
                        items:
                          type: string
                          example: "This value should not be blank."
                          
            '500':
              description: Server error
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
      /api/users/{id}:
        get:
          tags: [Users]
          summary: Get User by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the user
              schema:
                type: integer
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  example: |
                    {
                      "id": 1,
                      "username": "user1",
                      "roles": [
                        "ROLE_USER"
                      ]
                    }
            '404':
              description: User not found
              content:
                application/json:
                  example: |
                    {
                      "error": "User not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
        put:
          tags: [Users]
          summary: Update User by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the user
              schema:
                type: integer
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    username:
                      type: string
                      example: UpdatedUsername
                    password:
                      type: string
                      example: "123456"
          responses:
            '200':
              description: User updated successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      username:
                        type: string
                      roles:
                        type: array
                        items:
                          type: string
                      first_name:
                        type: string
                      last_name:
                        type: string
                      email:
                        type: string
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '404':
              description: User not found
              content:
                application/json:
                  example: |
                    {
                      "error": "User not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
        delete:
          tags: [Users]
          summary: Delete User by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the user
              schema:
                type: integer
          responses:
            '204':
              description: User deleted successfully
            '404':
              description: User not found
              content:
                application/json:
                  example: |
                    {
                      "error": "User not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
      /api/users/{id}/change-password:
        put:
          tags: [Users]
          summary: Change User Password by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the user
              schema:
                type: integer
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    password:
                      type: string
                      example: "123456"
                    password_confirm:
                      type: string
                      example: "123456"
          responses:
            '200':
              description: User password changed successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      message:
                        type: string
                        example: "Password changed"
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '404':
              description: User not found
              content:
                application/json:
                  example: |
                    {
                      "error": "User not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "Something went wrong, please try again later"
                    }
      /api/users/{id}/reservations:
        get:
          tags: [Users]
          summary: List User Reservations
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the user
              schema:
                type: integer
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                          example: 37
                        start_date:
                          type: string
                          example: "15/11/2024"
                        end_date:
                          type: string
                          example: "17/11/2024"
                        status:
                          type: string
                          example: "active"
                        car:
                          type: integer
                          example: 15
                
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
            '403':
              description: Forbidden (when user tries to access other user's reservations)
              content:
                application/json:
                  example: |
                    {
                      "error": "Access Denied"
                    }
      /api/reservations:
        get:
          tags: [Reservations]
          summary: List Reservations
          parameters:
            - name: page
              in: query
              required: false
              description: The page number
              schema:
                type: integer
                default: 1
            - name: limit
              in: query
              required: false
              description: The number of items per page
              schema:
                type: integer
                default: 10
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      items:
                        type: array
                        items:
                          type: object
                          properties:
                            id:
                              type: integer
                              example: 37
                            start_date:
                              type: string
                              example: "15/11/2024"
                            end_date:
                              type: string
                              example: "17/11/2024"
                            status:
                              type: string
                              example: "active"
                            car:
                              type: integer
                              example: 15
                            user:
                              type: integer
                              example: 1
                      total:
                        type: integer
                        example: 1
                    
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
        post:
          tags: [Reservations]
          summary: Create Reservation
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    start_date:
                      type: string
                      example: "15/11/2024"
                    end_date:
                      type: string
                      example: "17/11/2024"
                    car_id:
                      type: integer
                      example: 15
          responses:
            '201':
              description: Reservation created successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      start_date:
                        type: string
                      end_date:
                        type: string
                      status:
                        type: string
                      car:
                        type: integer
                      user:
                        type: integer
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '500':
              description: Server error
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '404':
              description: Car not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Car not found"
                    }
      /api/reservations/{id}:
        get:
          tags: [Reservations]
          summary: Get Reservation by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the reservation
              schema:
                type: integer
          responses:
            '200':
              description: Successful response
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                        example: 37
                      start_date:
                        type: string
                        example: "15/11/2024"
                      end_date:
                        type: string
                        example: "17/11/2024"
                      status:
                        type: string
                        example: "active"
                      car:
                        type: integer
                        example: 15
                      user:
                        type: integer
                        example: 1
            '404':
              description: Reservation not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Reservation not found"
                    }
            '403':
              description: Forbidden (when user tries to access other user's reservation)
              content:
                application/json:
                  example: |
                    {
                      "error": "Access Denied"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
        put:
          tags: [Reservations]
          summary: Update Reservation by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the reservation
              schema:
                type: integer
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    start_date:
                      type: string
                      example: "15/11/2024"
                    end_date:
                      type: string
                      example: "17/11/2024"
                    car:
                      type: integer
                      example: 15
          responses:
            '200':
              description: Reservation updated successfully
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      id:
                        type: integer
                      start_date:
                        type: string
                      end_date:
                        type: string
                      status:
                        type: string
                      car:
                        type: integer
                      user:
                        type: integer
            '400':
              description: Bad request
              content:
                application/json:
                  schema:
                    type: object
                    properties:
                      error:
                        type: string
            '404':
              description: Reservation not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Reservation not found"
                    }
        delete:
          tags: [Reservations]
          summary: Cancel Reservation by ID
          parameters:
            - name: id
              in: path
              required: true
              description: The ID of the reservation
              schema:
                type: integer
          responses:
            '204':
              description: Reservation canceled successfully
            '404':
              description: Reservation not found
              content:
                application/json:
                  example: |
                    {
                      "error": "Reservation not found"
                    }
            '500':
              description: Server error
              content:
                application/json:
                  example: |
                    {
                      "error": "string"
                    }
            '403':
              description: Forbidden (when user tries to cancel other user's reservation)
              content:
                application/json:
                  example: |
                    {
                      "error": "Access Denied"
                    }
        


