<?php
namespace App\Tests\Services;

use App\Entity\Reservation;
use App\Services\ReservationService;
use App\Services\CarService;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;


class ReservationServiceTest extends TestCase
{
    private $reservationService;
    private $validator;
    private $serializer;
    private $entityManager;
    private $carService;

    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->carService = $this->createMock(CarService::class);

        $this->reservationService = new ReservationService(
            $this->validator,
            $this->serializer,
            $this->entityManager,
            $this->carService
        );
    }

    public function testCreateReservation(): void
    {
        $requestData = '{"carId": 1, "startDate": "2022-01-01", "endDate": "2022-01-05"}';
        $reservation = new Reservation();
        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($requestData, Reservation::class, 'json', ['carService' => $this->carService])
            ->willReturn($reservation);

        $result = $this->reservationService->createReservation($requestData);

        $this->assertSame($reservation, $result);
    }

    public function testUpdateReservation(): void
    {
        $requestData = '{"carId": 1, "startDate": "2022-01-01", "endDate": "2022-01-05"}';
        $reservation = new Reservation();
        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($requestData, Reservation::class, 'json', ['carService' => $this->carService, 'object_to_populate' => $reservation])
            ->willReturn($reservation);

        $result = $this->reservationService->updateReservation($reservation, $requestData);

        $this->assertSame($reservation, $result);
    }

    public function testDeleteReservation(): void
    {
        $reservation = new Reservation();
        $this->entityManager->expects($this->once())
            ->method('remove')
            ->with($reservation);
        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->reservationService->deleteReservation($reservation);
    }

    public function testValidDateRange(): void
    {
        $currentDate = new \DateTimeImmutable();
        $startDate = $currentDate->modify('+1 day');
        $endDate = $currentDate->modify('+5 day');


        $this->expectNotToPerformAssertions();
        $this->reservationService->dateControl($startDate, $endDate);
    }

    public function testWithInvalidDateRange(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $currentDate = new \DateTimeImmutable();
        $startDate = $currentDate->modify('+1 day');
        $endDate = $currentDate->modify('-5 day');

        $this->reservationService->dateControl($startDate, $endDate);
    }

    public function testWithInvalidStartDate(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $currentDate = new \DateTimeImmutable();
        $startDate = $currentDate->modify('-1 day');
        $endDate = $currentDate->modify('+5 day');

        $this->reservationService->dateControl($startDate, $endDate);
    }

    public function testWithInvalidEndDate(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $currentDate = new \DateTimeImmutable();
        $startDate = $currentDate->modify('+1 day');
        $endDate = $currentDate->modify('-5 day');

        $this->reservationService->dateControl($startDate, $endDate);
    }

    protected function tearDown(): void
    {
        $this->reservationService = null;
        $this->validator = null;
        $this->serializer = null;
        $this->entityManager = null;
        $this->carService = null;
    }
}